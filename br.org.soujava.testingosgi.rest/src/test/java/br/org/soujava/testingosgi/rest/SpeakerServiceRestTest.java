package br.org.soujava.testingosgi.rest;

import java.io.InputStream;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;
import org.junit.Assert;
import org.junit.Test;

import br.org.soujava.testingosgi.model.Speaker;
import br.org.soujava.testingosgi.model.SpeakerService;

public class SpeakerServiceRestTest {
    
    private static final String PERSONSERVICE_TESTURL = "http://localhost:8282/speaker";


    @Test
    public void testPutSpeaker() {
        SpeakerService speakerService = new SpeakerServiceImpl();;
        Server server = startSpeakerService(speakerService);
        
        assert server.isStarted();

        WebClient client = WebClient.create(PERSONSERVICE_TESTURL + "/1001");

        InputStream is = this.getClass().getResourceAsStream("/speaker1.xml");
        client.put(is);

        Speaker speaker = speakerService.getSpeaker("1001");
        assertCorrectSpeaker(speaker);

        Speaker speaker2 = client.get(Speaker.class);
        assertCorrectSpeaker(speaker2);

        server.stop();

		assert server.isStarted() == false;
    }


    private void assertCorrectSpeaker(Speaker speaker) {
        Assert.assertNotNull(speaker);
        Assert.assertEquals("Thomas Modeneis", speaker.getName());
    }

    
    private Server startSpeakerService(SpeakerService speakerService) {
        JAXRSServerFactoryBean factory = new JAXRSServerFactoryBean();
        factory.setAddress(PERSONSERVICE_TESTURL);
        factory.setResourceClasses(SpeakerServiceImpl.class);
        factory.setResourceProvider(SpeakerServiceImpl.class, new SingletonResourceProvider(speakerService));
        Server server = factory.create();
        server.start();
        return server;
    }

}
