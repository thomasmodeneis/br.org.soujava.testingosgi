package br.org.soujava.testingosgi.rest;


import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;
import org.junit.Assert;
import org.junit.Test;

import br.org.soujava.testingosgi.model.Speaker;
import br.org.soujava.testingosgi.model.SpeakerService;

public class SpeakerServiceSOAPTest {
    
    public static final String PERSONSERVICE_TESTURL = "http://localhost:8282/speakerService";


    @Test
    public void testPutSpeaker() {
        SpeakerService speakerService = new SpeakerServiceImpl();;
        Server server = startSpeakerService(speakerService);
        
        JaxWsProxyFactoryBean proxy = new JaxWsProxyFactoryBean();
        proxy.setServiceClass(SpeakerService.class);
        proxy.setAddress(PERSONSERVICE_TESTURL);
        SpeakerService speakerServiceProxy = (SpeakerService)proxy.create();
        
        speakerServiceProxy.addSpeaker(createSpeaker());

        // Get from impl directly
        Speaker speaker = speakerService.getSpeaker("1001");
        assertCorrectSpeaker(speaker);

        // Get through proxy
        Speaker speaker2 = speakerServiceProxy.getSpeaker("1001");
        assertCorrectSpeaker(speaker2);

        server.stop();
    }


    public Speaker createSpeaker() {
        Speaker speaker = new Speaker();
        speaker.setId("1001");
        speaker.setName("Thomas Modeneis");
        return speaker;
    }


    public void assertCorrectSpeaker(Speaker speaker) {
        Assert.assertNotNull(speaker);
        Assert.assertEquals("Thomas Modeneis", speaker.getName());
    }

    
    public Server startSpeakerService(SpeakerService speakerService) {
        JaxWsServerFactoryBean factory = new JaxWsServerFactoryBean();
        factory.setAddress(PERSONSERVICE_TESTURL);
        factory.setServiceBean(speakerService);
        Server server = factory.create();
        server.start();
        return server;
    }

}