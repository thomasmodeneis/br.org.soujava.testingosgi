package br.org.soujava.testingosgi.rest;

import java.util.HashMap;
import java.util.Map;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.org.soujava.testingosgi.model.Speaker;
import br.org.soujava.testingosgi.model.SpeakerService;

@Produces(MediaType.APPLICATION_XML)
@WebService
public class SpeakerServiceImpl implements SpeakerService {
    Map<String, Speaker> speakerMap;
    
    public SpeakerServiceImpl() {
        speakerMap = new HashMap<String, Speaker>();
        Speaker speaker = newspeaker("1","Thomas Modeneis");
        speakerMap.put("1", speaker);
        
        speaker = newspeaker("2","Otavio Java");
        speakerMap.put("2", speaker);
        
    }

    Speaker newspeaker(String id,String name){
    	Speaker speaker = new Speaker();
        speaker.setId(id);
        speaker.setName(name);
        return speaker;
        
    }
    
    /* (non-Javadoc)
     * @see net.lr.tutorial.karaf.cxf.speakerservice.rest.SpeakerService#getAll()
     */
    @Override
    @GET
    @Path("/")
    public Speaker[] getAll() {
        return speakerMap.values().toArray(new Speaker[]{});
    }
    
    /* (non-Javadoc)
     * @see net.lr.tutorial.karaf.cxf.speakerservice.rest.SpeakerService#getSpeaker(java.lang.String)
     */
    @Override
    @GET
    @Path("/{id}")
    public Speaker getSpeaker(@PathParam("id") String id) {
        return speakerMap.get(id);
    }

    /* (non-Javadoc)
     * @see net.lr.tutorial.karaf.cxf.speakerservice.rest.SpeakerService#updateSpeaker(java.lang.String, net.lr.tutorial.karaf.cxf.speakerservice.speaker.Speaker)
     */
    @Override
    @PUT
    @Path("/{id}")
    public void updateSpeaker(@PathParam("id") String id, Speaker speaker) {
        speaker.setId(id);
        System.out.println("Update request received for " + speaker.getId() + " name:" + speaker.getName());
        speakerMap.put(id, speaker);
    }
    
    /* (non-Javadoc)
     * @see net.lr.tutorial.karaf.cxf.speakerservice.rest.SpeakerService#addSpeaker(net.lr.tutorial.karaf.cxf.speakerservice.speaker.Speaker)
     */
    @Override
    @POST
    @Path("/")
    public void addSpeaker(Speaker speaker) {
        System.out.println("Add request received for " + speaker.getId() + " name:" + speaker.getName());
        speakerMap.put(speaker.getId(), speaker);
    }

    
    
}