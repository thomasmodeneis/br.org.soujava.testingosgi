package br.org.soujava.testingosgi.rest;


import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;
import org.apache.cxf.jaxrs.provider.JAXBElementProvider;

/**
 * Start the rest service to test in the IDE
 *
 */
public class SpeakerServiceStarter {
    
    @SuppressWarnings("rawtypes")
    public void startRestService() {
        JAXRSServerFactoryBean factory = new JAXRSServerFactoryBean();
        factory.setAddress("http://localhost:8282/speaker");
        factory.setResourceClasses(SpeakerServiceImpl.class);
        factory.setResourceProvider(new SingletonResourceProvider(new SpeakerServiceImpl()));
        factory.setProvider(new JAXBElementProvider());
        Server server = factory.create();
        server.start();
    }


}