package br.org.soujava.testingosgi.model;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {
    private final static QName _Speaker_QNAME = new QName("http://br.org.soujava.testingosgi", "speaker");
    
    @XmlElementDecl(namespace = "http://br.org.soujava.testingosgi", name = "speaker")
    public JAXBElement<Speaker> createSpeaker(Speaker Speaker) {
        return new JAXBElement<Speaker>(_Speaker_QNAME, Speaker.class, Speaker);
    }
}