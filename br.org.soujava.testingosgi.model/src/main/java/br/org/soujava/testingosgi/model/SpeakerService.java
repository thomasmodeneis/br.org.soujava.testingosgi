package br.org.soujava.testingosgi.model;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Produces(MediaType.APPLICATION_XML)
@WebService
public interface SpeakerService {
    @GET
    @Path("/")
    public Speaker[] getAll();
    
    @GET
    @Path("/{id}")
    public Speaker getSpeaker(String id);
    
    @PUT
    @Path("/{id}")
    public void updateSpeaker(String id, Speaker Speaker);
    
    @POST
    @Path("/")
    public void addSpeaker(Speaker Speaker);
}