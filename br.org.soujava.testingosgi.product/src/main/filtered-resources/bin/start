#!/bin/bash
#
# https://williamhill.jira.com/browse/BOFS-3080
# This script was re-written to be able to handle restarts issued
# by Jenkins. Also have only one process running per server

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run as root" 1>&2
    exit 1
fi

dirname=`dirname "$0"`
progname="demo"

# Sourcing environment settings for karaf similar to tomcats setenvJAVA_HOME='/usr/java/jdk1.7.0_17'
#JAVA_MIN_MEM=2048M
JAVA_MAX_MEM=4096M
KARAF_SCRIPT="start"
export JAVA_MIN_MEM
export JAVA_MAX_MEM
export KARAF_SCRIPT
if [ -f "$dirname/setenv" ]; then
  . "$dirname/setenv"
fi

warn() {
    echo "${progname}: $*"
}

die() {
    warn "$*"
    exit 1
}

detectOS() {
    # OS specific support (must be 'true' or 'false').
    cygwin=false;
    darwin=false;
    aix=false;
    os400=false;
    case "`uname`" in
        CYGWIN*)
            cygwin=true
            ;;
        Darwin*)
            darwin=true
            ;;
        AIX*)
            aix=true
            ;;
        OS400*)
            os400=true
            ;;
    esac
    # For AIX, set an environment variable
    if $aix; then
         export LDR_CNTRL=MAXDATA=0xB0000000@DSA
         export IBM_JAVA_HEAPDUMP_TEXT=true
         echo $LDR_CNTRL
    fi
}

locateHome() {
    if [ "x$KARAF_HOME" != "x" ]; then
        warn "Ignoring predefined value for KARAF_HOME"
    fi

    # In POSIX shells, CDPATH may cause cd to write to stdout
    (unset CDPATH) >/dev/null 2>&1 && unset CDPATH

    KARAF_HOME=`cd "$dirname/.."; pwd`
    if [ ! -d "$KARAF_HOME" ]; then
        die "KARAF_HOME is not valid: $KARAF_HOME"
    fi
}

locateBase() {
    if [ "x$KARAF_BASE" != "x" ]; then
        if [ ! -d "$KARAF_BASE" ]; then
            die "KARAF_BASE is not valid: $KARAF_BASE"
        fi
    else
        KARAF_BASE=$KARAF_HOME
    fi
}

locateData() {
    if [ "x$KARAF_DATA" != "x" ]; then
        if [ ! -d "$KARAF_DATA" ]; then
            die "KARAF_DATA is not valid: $KARAF_DATA"
        fi
    else
        KARAF_DATA=$KARAF_BASE/data
    fi
}

getLock() {
    #Obtain file descriptor lock. This prevents more than one instance
    #being started.
    pidf=/tmp/${progname}.pid # define lock file name
    exec 221>${pidf}          # Open the file with descriptor 221
    flock --exclusive --nonblock 221 ||
    {
        # Lock aquistion failed
        echo "${pidf} already exists. If you are sure ${progname} is not runing then delete ${pidf}"
        exit 1
    }
    echo $$>&221
}

init() {
    # Determine if there is special OS handling we must perform
    detectOS

    # Check if other process is running
    getLock

    # Locate the Karaf home directory
    locateHome

    # Locate the Karaf base directory
    locateBase

    # Locate the Karaf data directory
    locateData
}

run() {
    if $cygwin; then
        KARAF_HOME=`cygpath --path --windows "$KARAF_HOME"`
        KARAF_BASE=`cygpath --path --windows "$KARAF_BASE"`
        KARAF_DATA=`cygpath --path --windows "$KARAF_DATA"`
        CLASSPATH=`cygpath --path --windows "$CLASSPATH"`
    fi
    # Ensure the log directory exists -- we need to have a place to redirect stdout/stderr
    if [ ! -d "$KARAF_DATA/log" ]; then
        mkdir -p "$KARAF_DATA/log"
    fi
    exec "$KARAF_HOME"/bin/karaf server "$@" >> "$KARAF_DATA/karaf.out" 2>&1 &
}

main() {
    init
    run "$@"
    sleep 2
}

main "$@"
