package br.org.soujava.testingosgi.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.org.soujava.testingosgi.model.Speaker;
import br.org.soujava.testingosgi.model.SpeakerService;


public class SpeakerServlet extends HttpServlet {
    private static final long serialVersionUID = -8444651625768440903L;
    private SpeakerService speakerService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletOutputStream os = resp.getOutputStream();
        Speaker[] speakers = speakerService.getAll();
        os.println("<html><body>");
        os.println("<h2>Speakers</h2>");
        os.println("<table>");
        os.println("<tr><th>Id</th><th>Name</th><th>URL</th></tr>");
        for (Speaker speaker : speakers) {
            String url = (speaker.getUrl() == null) ? "" : speaker.getUrl();
            os.println(String.format("<tr><td>%s</td><td>%s</td><td>%s</td></tr>", speaker.getId(), speaker.getName(), url));
        }
        os.println("</table>");
        os.println("<h2>Add Speaker</h2>");
        os.println("<form name='input' action='/speakerui' method='post'>");
        os.println("<table>");
        os.println("<tr><td>Id</td><td><input type='text' name='id'/></td></tr>");
        os.println("<tr><td>Name</td><td><input type='text' name='name'/></td></tr>");
        os.println("<tr><td>Homepage URL</td><td><input type='text' name='url'/></td></tr>");
        os.println("<tr><td colspan='2'><input type='submit' value='Add'/></td></tr>");
        os.println("</form>");
        os.println("</table>");
        os.println("</body></html>");
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String url = req.getParameter("url");
        Speaker speaker = new Speaker();
        speaker.setId(id);
        speaker.setName(name);
        speaker.setUrl(url);
        speakerService.addSpeaker(speaker );
        resp.sendRedirect("/speakerui");
    }

    public void setSpeakerService(SpeakerService speakerService) {
        this.speakerService = speakerService;
    }
    
    
}